(function() {  
  var addClass = function(className) {
    if (!this.className.match(className)) {
      this.className += " " + className;
    }      
  };

  var removeClass = function(className) {
    var classRegex = new RegExp("(?:^|\\s)" + className + "(?!\\S)", "g");
    this.className = this.className.replace(classRegex, '');
  };

  var displayIcon = function() {
    if (this.value.length === 0) {
      removeClass.call(this, 'clearable-x');
    }
    else {
      addClass.call(this, 'clearable-x');
    }
  };

  var hideIcon = function(e) {
    if (e.type !== 'mouseout' || this !== document.activeElement) {
      removeClass.call(this, 'clearable-x');
    }
  };

  var showPointer = function(e) {
    if (this.className.match("clearable-x")) {
      if (this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left) {
        addClass.call(this, 'clearable-on-x');
      }
      else {
        removeClass.call(this, 'clearable-on-x');
      }
    }
  };

  var clearInput = function(ev) {
    if (this.className.match("clearable-on-x")) {
      ev.preventDefault();
      removeClass.call(this, 'clearable-x');
      removeClass.call(this, 'clearable-on-x');
      this.value = '';
    }
  };

  document.querySelectorAll("input[type=text].clearable").forEach(
    function(input) {
      input.addEventListener("input", displayIcon);  
      input.addEventListener("mouseenter", displayIcon);      
      input.addEventListener("blur", hideIcon);
      input.addEventListener("mouseout", hideIcon);
      input.addEventListener("mousemove", showPointer);
      input.addEventListener("click", clearInput);    
    });
}());
