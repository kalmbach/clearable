Clearable
=========

A clear icon for your inputs.


Usage
-----

* Link the `clearable.css` stylesheet in your page
* Add the `.clearable` class to your text inputs
* Load the `clearable.js`in your page

Sample
------

See `index.html` file for a basic sample.

```
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="initial-scale=1, maximum-scale=1">
  <title>Clearable: a clear icon for your inputs</title>    
  <link href="normalize.css" rel="stylesheet">
  <link href="skeleton.css" rel="stylesheet">
  <link href="clearable.css" rel="stylesheet">
</head>
<body>
  <div class="container">
    <section style="margin-top: 10em; text-align: center;">      
      <label>Clearable Input</label>
      <input class='clearable' type="text" placeholder="test me"/>
    </section>    
  </div>
  <script type="text/javascript" src="clearable.js"></script>
</body>
</html>
```

License
-------

Clearable is released under the MIT License. See LICENSE file for details.